<?php

use Illuminate\Database\Seeder;

class FilmAndCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $films = [
            [
                'name' => 'The Godfather',
                'slug' => 'the-godfather',
                'description' => 'Classic Hollywood Movie',
                'release_date' => date("Y-m-d h:i:s"),
                'rating' => 5,
                'ticket_price' => 100,
                'country_id' => 2,
                'genre_id' => "[3]",
                'created_at' => date("Y-m-d h:i:s"),
                'updated_at' => date("Y-m-d h:i:s"),
            ],
            [
                'name' => 'Forrest Gump',
                'slug' => 'forrest-gump',
                'description' => 'Classic Hollywood Movie',
                'release_date' => date("Y-m-d h:i:s"),
                'rating' => 5,
                'ticket_price' => 120,
                'country_id' => 2,
                'genre_id' => "[4]",
                'created_at' => date("Y-m-d h:i:s"),
                'updated_at' => date("Y-m-d h:i:s"),
            ],
            [
                'name' => 'Shutter Island',
                'slug' => 'shutter-island',
                'description' => 'Shutter Island is a 2010 American neo-noir psychological thriller film directed by Martin Scorsese and written by Laeta Kalogridis, based on Dennis Lehane',
                'release_date' => date("Y-m-d h:i:s"),
                'rating' => 5,
                'ticket_price' => 120,
                'country_id' => 2,
                'genre_id' => "[3,2]",
                'created_at' => date("Y-m-d h:i:s"),
                'updated_at' => date("Y-m-d h:i:s"),
            ],
            [
                'name' => 'Avengers: Endgame',
                'slug' => 'avengers-endgame',
                'description' => 'Avengers: Endgame is a 2019 American superhero film based on the Marvel Comics superhero team the Avengers, produced by Marvel Studios and distributed by Walt Disney Studios Motion Pictures. It is the direct sequel to Avengers: Infinity War (2018) and the 22nd film in the Marvel Cinematic Universe (MCU).',
                'release_date' => date("Y-m-d h:i:s"),
                'rating' => 3,
                'ticket_price' => 10,
                'country_id' => 3,
                'genre_id' => "[3,2]",
                'created_at' => date("Y-m-d h:i:s"),
                'updated_at' => date("Y-m-d h:i:s"),
            ],
            [
                'name' => 'Joker',
                'slug' => 'joker',
                'description' => 'Joker is a 2019 American psychological thriller film directed and produced by Todd Phillips, who co-wrote the screenplay with Scott Silver. The film, based on DC Comics characters, stars Joaquin Phoenix as the Joker and provides a possible origin story for the character. Set in 1981, it follows Arthur Fleck, a failed stand-up comedian whose descent into insanity and nihilism inspires a violent counter-cultural revolution against the wealthy in a decaying Gotham City. Robert De Niro, Zazie Beetz, Frances Conroy, Brett Cullen, Glenn Fleshler, Bill Camp, Shea Whigham, and Marc Maron appear in supporting roles. Joker was produced by Warner Bros. Pictures, DC Films, and Joint Effort, in association with Bron Creative and Village Roadshow Pictures, and distributed by Warner Bros.',
                'release_date' => date("Y-m-d h:i:s"),
                'rating' => 5,
                'ticket_price' => 10,
                'country_id' => 1,
                'genre_id' => "[3,2,1]",
                'created_at' => date("Y-m-d h:i:s"),
                'updated_at' => date("Y-m-d h:i:s"),
            ],
            [
                'name' => 'Toy Story 4',
                'slug' => 'toy-story-4',
                'description' => 'Toy Story 4 is a 2019 American computer-animated comedy film produced by Pixar Animation Studios for Walt Disney Pictures. It is the fourth installment in Pixars Toy Story series and the sequel to Toy Story 3 (2010). It was directed by Josh Cooley from a screenplay by Andrew Stanton and Stephany Folsom; the three also conceived the story alongside John Lasseter, Rashida Jones, Will McCormack, Valerie LaPointe, and Martin Hynes. Tom Hanks, Tim Allen, Annie Potts, Joan Cusack, Don Rickles, Wallace Shawn, John Ratzenberger, Estelle Harris, Blake Clark, Bonnie Hunt, Jeff Garlin, Kristen Schaal and Timothy Dalton reprise their character roles from the first three films. They are joined by Tony Hale, Keegan-Michael Key, Jordan Peele, Christina Hendricks',
                'release_date' => date("Y-m-d h:i:s"),
                'rating' => 3,
                'ticket_price' => 10,
                'country_id' => 1,
                'genre_id' => "[1,2]",
                'created_at' => date("Y-m-d h:i:s"),
                'updated_at' => date("Y-m-d h:i:s"),
            ],
        ];
        DB::table('films')->insert($films);
        $comments = [
            ['name' => 'Mahady','comment' => 'Awesome Movie!', 'film_id' => 1, 'user_id' => 1],
            ['name' => 'Sohel','comment' => 'Awesome Movie Had a great time watching!', 'film_id' => 2, 'user_id' => 1],
            ['name' => 'Kamal','comment' => 'One of the Best movies of this era.', 'film_id' => 3, 'user_id' => 1],
            ['name' => 'Mizan','comment' => 'Another master piece by Marvels', 'film_id' => 4, 'user_id' => 1],
            ['name' => 'Raju','comment' => 'Proof that DC is still the best!', 'film_id' => 5, 'user_id' => 1],
            ['name' => 'Pial','comment' => 'One of the best Animated movies of all time.', 'film_id' => 6, 'user_id' => 1],
        ];
        DB::table('comments')->insert($comments);
    }
}
