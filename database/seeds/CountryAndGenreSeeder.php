<?php

use Illuminate\Database\Seeder;

class CountryAndGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            ['name' => 'Bangladesh','code' => 'BD'],
            ['name' => 'United States of America','code' => 'USA'],
            ['name' => 'India','code' => 'IND'],
            ['name' => 'England','code' => 'UK'],
        ];
        DB::table('countries')->insert($countries);
        $genres = [
            ['name' => 'Horror','description' => 'Horror Movies.'],
            ['name' => 'Animated','description' => 'Animated Movies'],
            ['name' => 'Drama','description' => 'Drama Movies'],
            ['name' => 'Fantasy','description' => 'Fantasy Movies'],
        ];
        DB::table('genres')->insert($genres);
    }
}
