<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Mr. Administrator',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'created_at' => date("Y-m-d h:i:s"),
            'updated_at' => date("Y-m-d h:i:s"),
        ]);
        Role::create(['name' => 'administrator']);
        Role::create(['name' => 'user']);
        $user = User::find(1);
        $user->assignRole(['administrator', 'user']);
    }
}
