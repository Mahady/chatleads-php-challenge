# ChatLeads PHP-Challenge

This is a Film Managment platform built with **Laravel 6.2**. This project is an assignment given by the recruiters at Chadleads.

### Installation

First clone the repository from gitlab.com. by executing this command.
```sh
$ git clone https://gitlab.com/Mahady/chatleads-php-challenge.git
```
Now go the the project directory and install the required dependencies
```sh
$ cd chatleads-php-challenge
$ composer install
$ npm install
$ npm run prod
```
Create a `.env` file and copy all the content from `.env-example` and paste inside the newly created `.env` file. 
Create a database named **chatleads**. And edit the values in the `.env` file associated to your Database if needed.
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=chatleads
DB_USERNAME=root
DB_PASSWORD=
```
Now run this command to create all necessary tables in the DB with seed data.
```sh
$ php artisan migrate --seed
```
Finally run this command to run the project.
```sh
$ php artisan serve
```
The project can be accesible by going to http://localhost:8000

**Access Admin Dashboard by loging in with these credentials:**
**`Email: admin@admin.com`**
**`Password: 123456`**

## Gist of what this Application does

This Application can be used to Create or View Films and commenting on them. Without logging in a **Guest** can browse through Films and view the details and read comments.

One can register by clicking `Login` button at the top menu. After logging-in an **User** can post comments to.

And to manage all these, A administrator account is created by default. After logging in **Admin** will be redirected to the Admin Dashboard where he can:

    - Create new Users and Admins
    - Manage Users and Admins
    - Ban Users and Admins
    - Manage Countries
    - Manage Genres

And in the FrontEnd Admin can do all the things an User can and on top of that Admin can Create new Movies.