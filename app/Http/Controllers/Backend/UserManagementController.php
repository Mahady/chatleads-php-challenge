<?php

namespace App\Http\Controllers\Backend;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;

class UserManagementController extends Controller
{
	public function index()
    {
        return view('backend.user-management.index');
    }

    public function create()
    {
        $roles = Role::get();
    	return view('backend.user-management.create',compact('roles'));
    }

    public function store(StoreUserRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        $user->assignRole($request->roles);
        return redirect()->route('admin.users.index');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::get();
        $existingRoles = array();
        foreach ($user->roles as $role)   array_push($existingRoles, $role->name);
        return view('backend.user-management.edit',compact('user','roles','existingRoles'));
    }

    public function update(UpdateUserRequest $request,$id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->update();
        $user->syncRoles($request->roles);
        return redirect()->route('admin.users.index');
    }

    public function delete($id)
    {  
        $user = User::find($id);
        $user->deleted_by = Auth::user()->id;
        $user->update();
        $user->delete();
        return redirect()->route('admin.users.index');
    }

    public function ban($id)
    {
        $user = User::find($id);
        $user->is_banned = !$user->is_banned;
        $user->update();
        return redirect()->route('admin.users.index');
    }

    protected function userData(Request $request)
    {
        $users = User::get();
        $datatables = Datatables::of($users);

        return $datatables
            ->addColumn('action', function ($user) {
                if($user->is_banned)
                    return '<a href="edit/'.$user->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>' . ' <a href="delete/'.$user->id.'" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure?\')"><i class="glyphicon glyphicon-delete"></i> Delete</a>' . ' <a href="ban/'.$user->id.'" class="btn btn-xs btn-success" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-ban"></i> Unban</a>';
                return '<a href="edit/'.$user->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>' . ' <a href="delete/'.$user->id.'" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure?\')"><i class="glyphicon glyphicon-delete"></i> Delete</a>' . ' <a href="ban/'.$user->id.'" class="btn btn-xs btn-warning" onclick="return confirm(\'Are you sure?\')"><i class="fa fa-ban"></i> Ban</a>';
            })
            ->editColumn('id',function($user){
                return '<b>'.$user->id.'.</b>';
            })
            ->editColumn('name',function($user){
                return $user->name;
            })
            ->setRowClass(function ($user) {
                return $user->is_banned == 0 ? 'alert-success' : 'alert-danger';
            })
            ->editColumn('created_at', function ($user) {
                return $user->created_at->format('d-M-Y');
            })
            ->rawColumns(['id','action'])
            ->make(true);
    }
}