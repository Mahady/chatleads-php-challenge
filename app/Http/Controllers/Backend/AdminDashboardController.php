<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    public function adminDashboard()
    {
    	return view('backend.dashboard.dashboard');
    }

    public function register()
    {
    	return view('backend.auth.register');
    }

    
}
