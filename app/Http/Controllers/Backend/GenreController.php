<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{
    public function index()
    {
        return view('backend.genre.index');
    }

    public function create()
    {
    	return view('backend.genre.create');
    }

    public function store(Request $request)
    {
        $genre = new Genre();
        $genre->name = $request->name;
        $genre->description = $request->description;
        $genre->save();
        return redirect()->route('admin.genre.index');
    }

    public function edit($id)
    {
        $genre = Genre::find($id);
        return view('backend.genre.edit',compact('genre'));
    }

    public function update(Request $request,$id)
    {
        $genre = Genre::find($id);
        $genre->name = $request->name;
        $genre->description = $request->description;
        $genre->update();
        return redirect()->route('admin.genre.index');
    }

    public function delete($id)
    {  
        $genre = Genre::find($id);
        $genre->delete();
        return redirect()->route('admin.genre.index');
    }


    protected function genreData(Request $request)
    {
        $genres = Genre::get();
        $datatables = Datatables::of($genres);

        return $datatables
            ->addColumn('action', function ($genre) {
                return '<a href="edit/'.$genre->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>' . ' <a href="delete/'.$genre->id.'" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure?\')"><i class="glyphicon glyphicon-delete"></i> Delete</a>';
            })
            ->editColumn('id',function($genre){
                return '<b>'.$genre->id.'.</b>';
            })
            ->editColumn('name',function($genre){
                return $genre->name;
            })
            ->rawColumns(['id','action'])
            ->make(true);
    }
}
