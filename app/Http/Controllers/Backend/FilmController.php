<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Country;
use App\Genre;
use App\Film;
use App\Comment;
use Auth;

class FilmController extends Controller
{
    public function index()
    {
        return view('frontend.film.index');
    }

    public function create()
    {
        $countries = Country::get();
        $genres = Genre::get();
        return view('frontend.film.create',compact('countries','genres'));
    }

    public function details($slug)
    {
        $film = Film::select('films.*','countries.name as country')
            ->where('films.slug',$slug)
            ->join('countries','countries.id','=','films.country_id')
            ->first();
        $genres = Genre::whereIn('id',json_decode($film->genre_id))->get();
        $film->getFirstMedia('FilmImage');
        if( count($film->media))
        $film->imgSrc = '/storage/' . $film->media[0]->id . '/' . $film->media[0]->file_name;
        $comments = Comment::where('film_id',$film->id)->get();
        if(Auth::check()) $isLoggedIn = 'true';
        else $isLoggedIn = 'false';
        return view('frontend.film.details',compact('film','comments','genres','isLoggedIn'));
    }
}
