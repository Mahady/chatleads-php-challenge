<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Country;

class CountryController extends Controller
{
    public function index()
    {
        return view('backend.country.index');
    }

    public function create()
    {
    	return view('backend.country.create');
    }

    public function store(Request $request)
    {
        $country = new Country();
        $country->name = $request->name;
        $country->code = $request->code;
        $country->save();
        return redirect()->route('admin.country.index');
    }

    public function edit($id)
    {
        $country = Country::find($id);
        return view('backend.country.edit',compact('country'));
    }

    public function update(Request $request,$id)
    {
        $country = Country::find($id);
        $country->name = $request->name;
        $country->code = $request->code;
        $country->update();
        return redirect()->route('admin.country.index');
    }

    public function delete($id)
    {  
        $country = Country::find($id);
        $country->delete();
        return redirect()->route('admin.country.index');
    }


    protected function countryData(Request $request)
    {
        $countries = Country::get();
        $datatables = Datatables::of($countries);

        return $datatables
            ->addColumn('action', function ($country) {
                return '<a href="edit/'.$country->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>' . ' <a href="delete/'.$country->id.'" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure?\')"><i class="glyphicon glyphicon-delete"></i> Delete</a>';
            })
            ->editColumn('id',function($country){
                return '<b>'.$country->id.'.</b>';
            })
            ->editColumn('name',function($country){
                return $country->name;
            })
            ->rawColumns(['id','action'])
            ->make(true);
    }
}
