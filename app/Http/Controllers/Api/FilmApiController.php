<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Film;
use App\Comment;

class FilmApiController extends Controller
{
    public function store(Request $request)
    {
        $film = new Film();
        $film->name = $request->name;
        $film->slug = str_replace(' ', '-', strtolower($request->name));
        $film->description = $request->description;
        $film->release_date = $request->release_date;
        $film->rating = $request->rating;
        $film->ticket_price = $request->ticket_price;
        $film->country_id = $request->country_id;
        $film->genre_id = $request->genre_id;
        $film->save();

        $binaryData = base64_decode($request->image);
        $png_url = $film->slug.".png";
        $path = tempnam(public_path(), $png_url);
        file_put_contents($path, $binaryData);
        $film->addMedia($path)->usingName($film->slug)->toMediaCollection('FilmImage');
        return response()->json($film,Response::HTTP_OK);
    }

    public function index()
    {
        $films = Film::paginate(3);
        foreach($films as $film){
            $film->getFirstMedia('FilmImage');
            $film->commentCount = Comment::where('film_id',$film->id)->count();
        }
        return response()->json($films,Response::HTTP_OK);
    }

    public function postComment(Request $request)
    {
        $comment = new Comment();
        $comment->name = $request->name;
        $comment->comment = $request->comment;
        $comment->film_id = $request->film_id;
        $comment->user_id = 1; //TODO
        $comment->save();
        return response()->json($comment,Response::HTTP_OK);
    } 
}
