<?php

function redirectAfterLogin()
{
	if(auth()->check()){
		if(auth()->user()->hasRole('administrator')) return 'admin.dashboard';
		if(auth()->user()->hasRole('user')) return 'root';
	}
	return 'root';
}