<!DOCTYPE html>
<html>
<body>
    <h1>Hi {{ $user->first_name }} {{ $user->last_name }}</h1>
    <h2>Welcome to Manhattan Project!</h2>
    <p>We are pleased to have you with us. Your journey with Manhattan is about to be the best experience you have ever had.</p>
    <p>Login to <a href="http://localhost:8000/login">Manhattan Project</a> and Enjoy!</p>
    <br><br>
    <b>Thank you,</b><br>
    <b>Manhattan Project Team</b>
</body>
</html>