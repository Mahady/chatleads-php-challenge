@extends('backend.master')

@section('title')
    Genre List
@endsection

@section('mainContent')
<div class="table-responsive">
    <table class="table table-bordered" id="category-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Genre Name</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
@stop
@push('scripts')
<script>
$(function() {
    $('#category-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.genre.datatables') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'description', name: 'description' },
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
});
</script>
@endpush