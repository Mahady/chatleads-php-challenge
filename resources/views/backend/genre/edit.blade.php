@extends('backend.master')

@section('title')
	Edit Genre
@endsection

@section('mainContent')

    <div class="container">
    <h3 class="text-center text-primary"> Edit Genre</h3>

            <h4 class="text-center text-success">{{Session::get('message')}}</h4>     
            <form class='form-horizontal' method="POST" action="{{ route('admin.genre.update',['id' => $genre->id]) }}" enctype="multipart/form-data">
            	@csrf
	          	<div class="form-group">
			      <label class="control-label col-sm-2" for="name">Genre Name:</label>
			      <div class="col-sm-10">
			        <input type="text" value="{{$genre->name}}" class="form-control" id="name" placeholder="Enter Genre Name" name="name" autofocus required>
				  </div>
			    </div>
				<div class="form-group">
			      <label class="control-label col-sm-2" for="code">Description:</label>
			      <div class="col-sm-10">
			        <input type="text"  value="{{$genre->description}}" class="form-control" id="description" placeholder="Enter Description" name="description" required>
				  </div>
			    </div>
			    
		    	<div class="form-group">
		            <div class="col-sm-offset-2 col-sm-10">
		                <a class="btn btn-danger" href="{{route('admin.genre.index')}}">Cancel</a>
		                <button type="submit" class="btn btn-success" name="sub">Update Genre</button>
		            </div>
		         </div>
        </form>
</div>
@endsection