<footer class="sticky-footer bg-white">
	<div class="container my-auto">
	  <div class="copyright text-center my-auto">
	    <span>Copyright &copy; Chatleads PHP-Challenge Project <b id="year"></b></span>
	  </div>
	</div>
</footer>

<script type="text/javascript">
	function myFunction() {
	  var d = new Date();
	  var n = d.getFullYear();
	  document.getElementById("year").innerHTML = n;
	}
	myFunction();
</script>