<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('admin.dashboard')}}">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fa fa-bug"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Chatleads Dashboard</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="{{route('admin.dashboard')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" target="_blank" href="{{route('root')}}">
          <i class="fa fa-laptop"></i>
          <span>View Frontend</span></a>
      </li>
      <!-- Divider -->
      @hasrole('administrator')

      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        User & Role Management
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fa fa-user-circle"></i>
          <span>Users</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Manage Users:</h6>
            <a class="collapse-item" href="{{route('admin.users.create')}}"><i class="fa fa-plus"></i> Add User</a>
            <a class="collapse-item" href="{{route('admin.users.index')}}"><i class="fa fa-cogs"></i> Manage Users</a>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fa fa-user-circle"></i>
          <span>Country & Genre</span>
        </a>
        <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Manage Country:</h6>
            <a class="collapse-item" href="{{route('admin.country.create')}}"><i class="fa fa-plus"></i> Add Country</a>
            <a class="collapse-item" href="{{route('admin.country.index')}}"><i class="fa fa-cogs"></i> Manage Country</a>
            <h6 class="collapse-header">Manage Genre:</h6>
            <a class="collapse-item" href="{{route('admin.genre.create')}}"><i class="fa fa-plus"></i> Add Genre</a>
            <a class="collapse-item" href="{{route('admin.genre.index')}}"><i class="fa fa-cogs"></i> Manage Genre</a>
          </div>
        </div>
      </li>
      @endhasrole
      <!-- Nav Item - Utilities Collapse Menu -->

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>