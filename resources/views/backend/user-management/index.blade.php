@extends('backend.master')

@section('title')
    User List
@endsection

@section('mainContent')
<!-- 
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">-->
                <!-- <h4 class="text-center text-success">{{Session::get('message')}}</h4>      -->
                <div class="table-responsive">
                    <table class="table table-bordered" id="category-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Member Since</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
      <!--       </div>
        </div>
    </div>
</div> -->
@stop
@push('scripts')
<script>
$(function() {
    // var i=1;
    $('#category-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.users.datatables') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'created_at', name: 'created_at' },
            {data: 'action', name: 'action', orderable: false, searchable: false},
            // { data: 'updated_at', name: 'updated_at' }
        ]
    });
});
</script>
@endpush