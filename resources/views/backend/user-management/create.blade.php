@extends('backend.master')

@section('title')
	Create User
@endsection

@section('mainContent')

    <div class="container">
    <h3 class="text-center text-primary"> Create User</h3>

            <h4 class="text-center text-success">{{Session::get('message')}}</h4>     
            <form class='form-horizontal' method="POST" action="{{ route('admin.users.store') }}" enctype="multipart/form-data">
            	@csrf
	          	<div class="form-group">
			      <label class="control-label col-sm-2" for="name">Name:</label>
			      <div class="col-sm-10">
			        <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" autofocus required>
					@error('name')
						<span role="alert" strong style="color:red">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				  </div>
			    </div>
			    <div class="form-group">
			      <label class="control-label col-sm-2" for="email">Email:</label>
			      <div class="col-sm-10">
			        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
					@error('email')
						<span role="alert" strong style="color:red">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				  </div>
			    </div>
			    <div class="form-group">
			      <label class="control-label col-sm-2" for="pwd">Password:</label>
			      <div class="col-sm-10">
			        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="password">
					@error('password')
						<span role="alert" strong style="color:red">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				  </div>
			    </div>

			    <div class="form-group">
			      <label class="control-label col-sm-2" for="pwd">Roles:</label>
			      @foreach($roles as $role)  
			        <input type="checkbox" name="roles[]" value="{{$role->name}}" {{$role->name == 'user' ? 'checked' : ''}}/> {!! $role->name !!} 
			      @endforeach
			    </div>


		    	<div class="form-group">
		            <div class="col-sm-offset-2 col-sm-10">
		                <a class="btn btn-danger" href="{{route('admin.users.index')}}">Cancel</a>
		                <button type="submit" class="btn btn-success" name="sub">Create User</button>
		            </div>
		         </div>
        </form>
</div>
@endsection