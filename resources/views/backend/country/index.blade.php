@extends('backend.master')

@section('title')
    User List
@endsection

@section('mainContent')
<div class="table-responsive">
    <table class="table table-bordered" id="category-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Country Name</th>
                <th>Country Code</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
@stop
@push('scripts')
<script>
$(function() {
    $('#category-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('admin.country.datatables') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'code', name: 'code' },
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
});
</script>
@endpush