@extends('backend.master')

@section('title')
	Create Country
@endsection

@section('mainContent')

    <div class="container">
    <h3 class="text-center text-primary"> Create Country</h3>

            <h4 class="text-center text-success">{{Session::get('message')}}</h4>     
            <form class='form-horizontal' method="POST" action="{{ route('admin.country.store') }}" enctype="multipart/form-data">
            	@csrf
	          	<div class="form-group">
			      <label class="control-label col-sm-2" for="name">Country Name:</label>
			      <div class="col-sm-10">
			        <input type="text" class="form-control" id="name" placeholder="Enter Country Name" name="name" autofocus required>
				  </div>
			    </div>
				<div class="form-group">
			      <label class="control-label col-sm-2" for="code">Country Code:</label>
			      <div class="col-sm-10">
			        <input type="text" class="form-control" id="code" placeholder="Enter Country Code" name="code" required>
				  </div>
			    </div>
			    
		    	<div class="form-group">
		            <div class="col-sm-offset-2 col-sm-10">
		                <a class="btn btn-danger" href="{{route('admin.country.index')}}">Cancel</a>
		                <button type="submit" class="btn btn-success" name="sub">Create Country</button>
		            </div>
		         </div>
        </form>
</div>
@endsection