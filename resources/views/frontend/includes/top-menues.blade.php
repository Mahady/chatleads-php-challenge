@if (Route::has('login'))
    <div class="top-right links">
        @auth
            <a><b>{{Auth::user()->name}}</b></a>
            <a href="{{route('film.create')}}">Create Films</a>
            <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @else
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
                <a href="{{ route('register') }}">Register</a>
            @endif
        @endauth
        <a href="{{route('film.create')}}">Create Films</a>
    </div>
@endif