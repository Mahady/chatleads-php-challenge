@extends('master')

@section('title')
	Create Film
@endsection

@section('content')
<film-details
    :film = "{{ $film }}"
    :comments = "{{ $comments }}"
    :genres = "{{ $genres }}"
    :isloggedin = "{{ $isLoggedIn }}"
>

</film-details>
@endsection