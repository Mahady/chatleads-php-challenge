@extends('master')

@section('title')
	Create Film
@endsection

@section('content')
<create-film
:countries = "{{ $countries }}"
:genres = "{{ $genres }}"
>
</create_film>
@endsection
