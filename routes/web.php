<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Backend\WelcomeController@index')->name('root');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/films','Backend\FilmController@index')->name('film.index');
Route::get('/films/{slug}','Backend\FilmController@details')->name('film.details');

Route::group(['prefix' => 'admin','as'=>'admin.','middleware' => ['role:administrator']], function () {

	Route::get('/dashboard','Backend\AdminDashboardController@adminDashboard')->name('dashboard');

	/*.............................START - User Management - START............................*/
	Route::group(['prefix' => 'users','as' => 'users.'], function () {
		Route::get('/list','Backend\UserManagementController@index')->name('index');
		Route::get('/create','Backend\UserManagementController@create')->name('create');
		Route::post('/store','Backend\UserManagementController@store')->name('store');
		Route::get('/edit/{id}','Backend\UserManagementController@edit')->name('edit');
		Route::post('/update/{id}','Backend\UserManagementController@update')->name('update');
		Route::get('/delete/{id}','Backend\UserManagementController@delete')->name('delete');
		Route::get('/ban/{id}','Backend\UserManagementController@ban')->name('ban');
		Route::get('/datatable/users','Backend\UserManagementController@userData')->name('datatables');
	});
	/*...............................END - User Management - END..............................*/

	/*.............................START - Country Management - START............................*/
	Route::group(['prefix' => 'country','as' => 'country.'], function () {
		Route::get('/list','Backend\CountryController@index')->name('index');
		Route::get('/create','Backend\CountryController@create')->name('create');
		Route::post('/store','Backend\CountryController@store')->name('store');
		Route::get('/edit/{id}','Backend\CountryController@edit')->name('edit');
		Route::post('/update/{id}','Backend\CountryController@update')->name('update');
		Route::get('/delete/{id}','Backend\CountryController@delete')->name('delete');
		Route::get('/datatable/countries','Backend\CountryController@countryData')->name('datatables');
	});
	/*...............................END - Country Management - END..............................*/

	/*.............................START - Genre Management - START............................*/
	Route::group(['prefix' => 'genre','as' => 'genre.'], function () {
		Route::get('/list','Backend\GenreController@index')->name('index');
		Route::get('/create','Backend\GenreController@create')->name('create');
		Route::post('/store','Backend\GenreController@store')->name('store');
		Route::get('/edit/{id}','Backend\GenreController@edit')->name('edit');
		Route::post('/update/{id}','Backend\GenreController@update')->name('update');
		Route::get('/delete/{id}','Backend\GenreController@delete')->name('delete');
		Route::get('/datatable/genres','Backend\GenreController@genreData')->name('datatables');
	});
	/*...............................END - Genre Management - END..............................*/
});
Route::group(['middleware' => ['role:administrator']], function () {
	/*.............................START - Genre Management - START............................*/
	Route::group(['prefix' => 'film','as' => 'film.'], function () {
		Route::get('/create','Backend\FilmController@create')->name('create');
	});
	/*...............................END - Genre Management - END..............................*/
});